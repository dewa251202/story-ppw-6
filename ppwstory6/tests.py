from django.test import TestCase, Client
from main.models import Activity, Person
from main.forms import AddActivityForm, AddPersonForm

class UnitTestMyWeb(TestCase):
	def setUp(self):
		Activity.objects.create(id=0, activity_name="ACTIVITY_TEST", date="1970-01-01", time_start="00:00", time_end="12:00", place = "PLACE_TEST")
		Person.objects.create(id=0, name="PERSON_TEST", activity=Activity.objects.get(activity_name="ACTIVITY_TEST"))
	
	def test_can_access_index(self):
		response = Client().get('/')
		
		self.assertEqual(response.status_code, 200)
		
	def test_index_uses_correct_template(self):
		response = Client().get('/')
		
		self.assertTemplateUsed(response, 'main/index.html')
		
	def test_app_icon_loaded(self):
		response = Client().get('/')
		html = response.content.decode('utf8')
		
		self.assertIn("app_icon", html)
	
	def test_app_label_exist(self):
		response = Client().get('/')
		html = response.content.decode('utf8')
		
		self.assertIn("Daftar Kegiatan", html)
		
	def test_add_activity_icon_loaded(self):
		response = Client().get('/')
		html = response.content.decode('utf8')
		
		self.assertIn("add_activity_icon", html)
		
	def test_activity_card_loaded(self):
		response = Client().get('/')
		html = response.content.decode('utf8')
		
		self.assertIn("activity-card", html)
		
	def test_activity_model_successfully_created(self):
		self.assertEqual(Activity.objects.all().count(), 1)
		
	def test_person_model_successfully_created(self):
		self.assertEqual(Person.objects.all().count(), 1)
		
	def test_add_activity_form_url_loaded(self):
		response = Client().get('/add-activity-request')
		html = response.content.decode('utf8')
		form = AddActivityForm()
		
		self.assertEqual(response.status_code, 200)
		for x in form.fields.values():
			self.assertIn(x.label, html)
		
	def test_add_activity_form_working(self):
		args = {'activity_name' : "ACTIVITY_TEST", 
				'date' : "1970-01-01", 
				'time_start' : "00:00", 
				'time_end' : "12:00", 
				'place' : "PLACE_TEST", 
				'name' : "PERSON_TEST"}
		response = Client().get('/add-activity', args, follow=True)
		html = response.content.decode('utf8')
		
		self.assertRedirects(response, '/')
		self.assertIn("ACTIVITY_TEST", html)
		self.assertIn("PLACE_TEST", html)
		self.assertIn("PERSON_TEST", html)
		
	def test_add_person_form_url_loaded(self):
		response = Client().get('/add-person-request/' + str(0))
		html = response.content.decode('utf8')
		form = AddPersonForm()
		
		self.assertEqual(response.status_code, 200)
		for x in form.fields.values():
			self.assertIn(x.label, html)
		
	def test_add_person_exist(self):
		response = Client().get('/add-person/' + str(0), {'name' : "PERSON_TEST"}, follow=True)
		html = response.content.decode('utf8')
		
		self.assertRedirects(response, '/')
		self.assertIn("PERSON_TEST", html)