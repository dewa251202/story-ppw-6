# Generated by Django 3.1.2 on 2020-10-24 16:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_remove_person_activity'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='person',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='main.person'),
        ),
    ]
