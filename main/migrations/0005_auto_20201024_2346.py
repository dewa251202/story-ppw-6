# Generated by Django 3.1.2 on 2020-10-24 16:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_activity_person'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activity',
            name='person',
        ),
        migrations.AddField(
            model_name='person',
            name='activity',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='main.activity'),
        ),
    ]
