from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Activity, Person
from .forms import AddActivityForm, AddPersonForm
from datetime import datetime

context = {
	"activity_dict" : None,
	"add_activity_form" : None,
	"owner_form" : None,
	"add_activity_form_is_set" : None,
	"add_person_form" : None,
	"add_person_form_is_set" : None,
	"add_person_form_activity_id" : None
}

def reset_context():
	for key in context.keys():
		if key != "activity_dict":
			context[key] = None

def index(request):
	reset_context()

	activity_list = Activity.objects.all()
	activity_dict = dict()

	for activity in activity_list:
		activity_dict[activity] = Person.objects.filter(activity=activity)
		
	context["activity_dict"] = activity_dict
	
	return render(request, 'main/index.html', context)
	
def add_activity_request(request):
	reset_context()
	
	today = datetime.today()
	add_activity_form = AddActivityForm(initial={'date': today.date, 'time_start': today.time, 'time_end': today.time})
	
	context["add_activity_form"] = add_activity_form
	context["owner_form"] = AddPersonForm()
	context["add_activity_form_is_set"] = True
	
	return render(request, 'main/index.html', context)
	
def add_activity(request):
	add_activity_form = AddActivityForm(request.GET or None)
	owner_name = request.GET["name"]
	
	if request.method == 'GET':
		add_activity_form.save()
		Person.objects.create(activity=add_activity_form.instance, name=owner_name)
	
	return HttpResponseRedirect('/')
	
def add_person_request(request, activity_id):
	reset_context()

	context["add_person_form"] = AddPersonForm()
	context["add_person_form_activity_id"] = activity_id
	context["add_person_form_is_set"] = True
	
	return render(request, 'main/index.html', context)
	
def add_person(request, activity_id):
	owner_name = request.GET["name"]
	context["add_person_form_is_set"] = False
	
	if request.method == 'GET':
		Person.objects.create(activity=Activity.objects.get(id=activity_id), name=owner_name)
	
	return HttpResponseRedirect('/')