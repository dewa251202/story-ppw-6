from django import forms
from .models import Activity, Person
from django.contrib.admin import widgets     

class DateInput(forms.DateInput):
	input_type = 'date'
	
class TimeInput(forms.TimeInput):
	input_type = 'time'

class AddActivityForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.label_suffix = ""

	class Meta:
		model = Activity
		fields ='__all__'
	
	activity_name = forms.CharField(label="Nama", max_length=200)
	date = forms.DateField(label="Tanggal", widget=DateInput())
	time_start = forms.TimeField(label="Mulai", widget=TimeInput())
	time_end = forms.TimeField(label="Berakhir", widget=TimeInput())
	place = forms.CharField(label="Tempat", max_length=200)
	
class AddPersonForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.label_suffix = ""
	
	class Meta:
		model = Person
		fields = ["name"]
	
	name = forms.CharField(label="Nama Peserta", max_length=200)