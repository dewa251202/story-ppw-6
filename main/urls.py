from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('add-activity', views.add_activity, name='add-activity'),
    path('add-activity-request', views.add_activity_request, name='add-activity-request'),
	path('add-person/<int:activity_id>', views.add_person, name='add-person'),
    path('add-person-request/<int:activity_id>', views.add_person_request, name='add-person-request'),
]
