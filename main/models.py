from django.db import models

class Activity(models.Model):
	activity_name = models.CharField(max_length=200)
	date = models.DateField()
	time_start = models.TimeField()
	time_end = models.TimeField()
	place = models.CharField(max_length=200)

class Person(models.Model):
	activity = models.ForeignKey(Activity, on_delete=models.CASCADE, default="")
	name = models.CharField(max_length=100)